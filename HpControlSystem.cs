using System.Collections;
using UnityEngine;
using UnityEngine.UIElements;

public class HPcontrol : MonoBehaviour
{
    public static HPcontrol Instance;

    public delegate void HPHandler(object sender, float oldHPValue, float newHPValue);
    public event HPHandler OnHPValueChangedEvent;

    public void Awake()
    {
        Instance = this;
    }

    public float HP
    {
        get => PlayerPrefs.GetFloat("HP", 50f);
        private set => PlayerPrefs.SetFloat("HP", value);
    }
    public float MaxHP
    {
        get => PlayerPrefs.GetFloat("MaxHP", 50f);
        private set => PlayerPrefs.SetFloat("MaxHP", value);
    }

    public void SubtractionHPValue(object sender, float Value, float maxHP)
    {
        if (HP - Value < 0)
        {
            UpdateHPValue(sender, HP, 0, maxHP);
        }
        else
        {
            UpdateHPValue(sender, HP, HP - Value, maxHP);
        }

    }


    private IEnumerator Wait(float waitTime)
    {
        rand = Random.Range(0, mob.Length);
        var objs = GameObject.FindGameObjectsWithTag("Mob");
        Destroy(objs[objs.Length - 1]);

        yield return new WaitForSeconds(waitTime / 2);

        Instantiate(mob[rand], new Vector2(0, 0), Quaternion.identity);
    }

    private void UpdateHPValue(object sender, float oldHPValue, float newHPValue, float maxHP)

    {
        HP = newHPValue;

        if (HP == 0)
        {
            OnHPZeroValueEvent?.Invoke(sender, maxHP);

            PlayerPrefs.SetFloat("MaxHP", maxHP);
            PlayerPrefs.SetFloat("HP", maxHP);
        }
        OnHPValueChangedEvent?.Invoke(sender, oldHPValue, newHPValue);

    }


}