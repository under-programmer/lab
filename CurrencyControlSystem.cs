using UnityEngine;

public class Bank : MonoBehaviour
{
    public static Bank Instance;

    public delegate void MoneyHandler(object sender, float oldCurrencyValue, float newCurrencyValue);
    public event MoneyHandler OnMoneyValueChangedEvent;

    public delegate void GemHandler(object sender, float oldCurrencyValue, float newCurrencyValue);
    public event GemHandler OnGemValueChangedEvent;

    private void Awake()
    {
        Instance = this;
    }

    public float Money
    {
        get => PlayerPrefs.GetFloat("Money", 0);
        private set => PlayerPrefs.SetFloat("Money", value);
    }

    public float Gem
    {
        get => PlayerPrefs.GetFloat("Gem", 0);
        private set => PlayerPrefs.SetFloat("Gem", value);
    }

    public void AddMoneyValue(object sender, float Value)
    {
        UpdateMoneyValue(sender, Money, Money + Value);
    }

    public void SubtractionMoneyValue(object sender, float Value)
    {
        UpdateMoneyValue(sender, Money, Money - Value);
    }

    private void UpdateMoneyValue(object sender, float oldCurrencyValue, float newCurrencyValue)
    {
        Money = newCurrencyValue;

        OnMoneyValueChangedEvent?.Invoke(sender, oldCurrencyValue, newCurrencyValue);
    }

    public void AddGemValue(object sender, float Value)
    {
        UpdateGemValue(sender, Gem, Gem + Value);
    }

    public void SubtractionGemValue(object sender, float Value)
    {
        UpdateGemValue(sender, Gem, Gem - Value);
    }

    private void UpdateGemValue(object sender, float oldCurrencyValue, float newCurrencyValue)
    {
        Gem = newCurrencyValue;

        OnGemValueChangedEvent?.Invoke(sender, oldCurrencyValue, newCurrencyValue);
    }

}